'use strict'
const {
  Attribute,
} = require('./../models/index')

let index = async(request) => {
  let filter = {}

  if (request.query.list !== undefined) {
    filter.where = {
      id: JSON.parse(request.query.list),
    }
  }

  return await Attribute.findAll(filter)
    .then(products => {
      return products
    })
    .catch(err => {
      return err
    })
}

let store = async(request) => {
  return await Attribute.create({
    name: request.payload.name,
    order: request.payload.order,
  })
    .then(product => {
      return product
    })
    .catch(err => {
      return err
    })
}

let show = async(request) => {
  return await Attribute.findByPk(request.params.id)
    .then(product => {
      return product
    })
    .catch(err => {
      return err
    })
}

let update = async(request) => {
  return await Attribute.update({
    name: request.payload.name,
    order: request.payload.order,
  }, {
    where: {
      id: request.params.id,
    },
  })
    .then(product => {
      return product
    })
    .catch(err => {
      return err
    })
}

let destroy = async(request) => {
  return await Attribute.destroy({
    where: {
      id: request.payload.id,
    },
  })
    .then(() => {
      return {
        success: true,
        message: 'Produit supprimé',
      }
    })
    .catch(err => {
      return err
    })
}

module.exports = {
  index,
  store,
  show,
  update,
  destroy,
}

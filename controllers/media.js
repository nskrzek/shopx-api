const { Media } = require('./../models/index')

let index = async() => {
  return await Media.findAll()
    .then(products => {
      return products
    })
    .catch(err => {
      return err
    })
}

let store = async(request) => {
  return await Media.create({
    name: request.payload.name,
    description: request.payload.description,
    price: request.payload.price,
    quantity: request.payload.quantity,
  })
    .then(product => {
      return product
    })
    .catch(err => {
      return err
    })
}

let show = async(request) => {
  return await Media.findByPk(request.params.id)
    .then(product => {
      return product
    })
    .catch(err => {
      return err
    })
}

let update = async(request) => {
  return await Media.update({
    name: request.payload.name,
    description: request.payload.description,
    price: request.payload.price,
    quantity: request.payload.quantity,
  },
  {
    where: { id: request.params.id },
  })
    .then(product => {
      return product
    })
    .catch(err => {
      return err
    })
}

let destroy = async(request) => {
  return await Media.destroy({
    where: { id: request.params.id },
  })
    .then(() => {
      return {
        success: true,
        message: 'Produit supprimé',
      }
    })
    .catch(err => {
      return err
    })
}

module.exports = {
  index,
  store,
  show,
  update,
  destroy,
}

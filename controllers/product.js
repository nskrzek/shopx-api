const slugify = require('slugify')
const {
  Product,
  Attribute,
} = require('./../models/index')

let index = async() => {
  return await Product.findAll()
    .then(products => {
      return products
    })
    .catch(err => {
      return err
    })
}

let store = async(request) => {
  let next = Product.max('id') + 1

  return await Product.create({
    name: request.payload.name,
    slug: `${slugify(request.payload.name, { lower: true })  }-${  next}`,
    description: request.payload.description,
    price: request.payload.price,
    quantity: request.payload.quantity,
    quantity_min: request.payload.quantity_min,
    discount: request.payload.discount,
    active: request.payload.active,
  })
    .then(product => {
      return product
    })
    .catch(err => {
      return err
    })
}

let show = async(request) => {
  return await Product.findOne({
    include: {
      model: Attribute,
      attributes: ['name', 'order'],
      through: {
        as: 'with',
        attributes: ['value'],
      },
    },
    where: {
      slug: request.params.slug,
    },
  })
    .then(product => {
      return product
    })
    .catch(err => {
      return err
    })
}

let update = async(request) => {
  return await Product.update({
    name: request.payload.name,
    slug: `${slugify(request.payload.name, { lower: true })  }-${  request.params.id}`,
    description: request.payload.description,
    price: request.payload.price,
    quantity: request.payload.quantity,
    quantity_min: request.payload.quantity_min,
    discount: request.payload.discount,
    active: request.payload.active,
  }, {
    where: {
      id: request.params.id,
    },
  })
    .then(product => {
      return product
    })
    .catch(err => {
      return err
    })
}

let destroy = async(request) => {
  return await Product.destroy({
    where: {
      id: request.params.id,
    },
  })
    .then(() => {
      return {
        success: true,
        message: 'Produit supprimé',
      }
    })
    .catch(err => {
      return err
    })
}

module.exports = {
  index,
  store,
  show,
  update,
  destroy,
}

'use strict'
const { User } = require('./../models/index')

let index = async() => {
  return await User.findAll()
    .then(products => {
      return products
    })
    .catch(err => {
      return err
    })
}

let store = async(request) => {
  return await User.create({
    firstName: request.payload.firstName,
    lastName: request.payload.lastName,
    email: request.payload.email,
    password: request.payload.password,
  })
    .then(product => {
      return product
    })
    .catch(err => {
      return err
    })
}

let show = async(request) => {
  return await User.findByPk(request.params.id)
    .then(product => {
      return product
    })
    .catch(err => {
      return err
    })
}

let update = async(request) => {
  return await User.update({
    firstName: request.payload.firstName,
    lastName: request.payload.lastName,
    email: request.payload.email,
    password: request.payload.password,
  },
  {
    where: { id: request.params.id },
  })
    .then(product => {
      return product
    })
    .catch(err => {
      return err
    })
}

let destroy = async(request) => {
  return await User.destroy({
    where: { id: request.params.id },
  })
    .then(() => {
      return {
        success: true,
        message: 'Produit supprimé',
      }
    })
    .catch(err => {
      return err
    })
}

module.exports = {
  index,
  store,
  show,
  update,
  destroy,
}

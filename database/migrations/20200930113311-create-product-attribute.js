'use strict'
module.exports = {
  up: async(queryInterface, Sequelize) => {
    await queryInterface.createTable('ProductAttributes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      ProductId: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      AttributeId: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      value: {
        type: Sequelize.TEXT,
      },
    }, { timestamps: false })
  },
  down: async(queryInterface) => {
    await queryInterface.dropTable('ProductAttributes')
  },
}

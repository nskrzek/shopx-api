'use strict'
const {
  Model,
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class Attribute extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      // let {Attribute, ProductAttribute, Product} = models
      // Attribute.belongsToMany(Product,{ through: ProductAttribute})
    }
  }
  Attribute.init({
    name: DataTypes.STRING,
    order: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'Attribute',
    timestamps: false,
  })

  return Attribute
}

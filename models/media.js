'use strict'
const {
  Model,
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class Media extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      let { Product, Media } = models
      Media.belongsTo(Product)
    }
  }
  Media.init({
    title: DataTypes.STRING,
    description: DataTypes.TEXT,
    width: DataTypes.INTEGER,
    height: DataTypes.INTEGER,
    type: DataTypes.STRING,
    path: DataTypes.STRING,
    product_id: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'Media',
  })

  return Media
}

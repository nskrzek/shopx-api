'use strict'
const {
  Model,
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class ProductAttribute extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }

  ProductAttribute.init({
    ProductId: DataTypes.INTEGER,
    AttributeId: DataTypes.INTEGER,
    value: DataTypes.TEXT,
  }, {
    sequelize,
    modelName: 'ProductAttribute',
  })

  return ProductAttribute
}

const ProductController = require('./controllers/product')
const AttributeController = require('./controllers/attribute')

module.exports = [{
  method: 'GET',
  path: '/',
  handler: (req, h) => {
    return {
      message: 'Space invader are coming !',
    }
  },
},
// Products
{
  method: 'GET',
  path: '/products',
  handler: ProductController.index,
},
{
  method: 'POST',
  path: '/products',
  handler: ProductController.store,
},
{
  method: 'GET',
  path: '/products/{slug}',
  handler: ProductController.show,
},
{
  method: ['PUT', 'PATCH'],
  path: '/products/{id}',
  handler: ProductController.update,
},
{
  method: 'DELETE',
  path: '/products/{id}',
  handler: ProductController.destroy,
},
// Attributes
{
  method: 'GET',
  path: '/attribute',
  handler: AttributeController.index,
},
{
  method: 'POST',
  path: '/attribute',
  handler: AttributeController.store,
},
{
  method: 'GET',
  path: '/attribute/{id}',
  handler: AttributeController.show,
},
{
  method: ['PUT', 'PATCH'],
  path: '/attribute/{id}',
  handler: AttributeController.update,
},
{
  method: 'DELETE',
  path: '/attribute/{id}',
  handler: AttributeController.destroy,
},
]

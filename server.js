'use strict'

const Hapi = require('@hapi/hapi')

const routes = require('./routes')

const env = process.env.NODE_ENV || 'development'
const settings = require(`${__dirname  }/config/${env}.json`).server

const init = async() => {
  const server = new Hapi.Server(settings)

  server.route(routes)

  await server.start()
  console.log('Server running on %s', server.info.uri)
}

process.on('unhandledRejection', (err) => {
  console.error(err)
  // eslint-disable-next-line no-process-exit
  process.exit(1)
})

init()
